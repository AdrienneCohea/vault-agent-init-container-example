# Quick start

```
# Simulate having a Vault cluster with reasonable access control policies and auth roles...

systemd-run --user vault server -dev -dev-root-token-id="root"
terraform init
terraform apply

# Then we would run any file-based secrets injection via an init container using the official Vault image (for example, hashicorp/vault:1.9.0)

vault agent -config=agent.hcl
```

# Verify the result

```bash
$ cat /tmp/my-service.properties
property=value
some_other_property=another value
yet_another_property=still yet another value
```

# How to customize for your own use case

The `agent.hcl` file has the configuration for where to find Vault and how to authenticate. The `properties` file is the template to be rendered with secrets in the container's tmpfs.
