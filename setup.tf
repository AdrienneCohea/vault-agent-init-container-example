resource "vault_auth_backend" "approle" {
  type = "approle"
}

resource "vault_approle_auth_backend_role" "my-service" {
  role_name             = "my-service"
  token_policies        = ["default", vault_policy.my-service.name]
  secret_id_bound_cidrs = []
  token_bound_cidrs     = []
  backend               = vault_auth_backend.approle.path
}

resource "vault_policy" "my-service" {
  name   = "service-my-service"
  policy = data.vault_policy_document.my-service.hcl
}

data "vault_policy_document" "my-service" {
  rule {
    path         = "kv/my-service"
    capabilities = ["list", "read"]
    description  = "List secrets"
  }

  rule {
    path         = "kv/my-service/*"
    capabilities = ["list"]
    description  = "Read secrets"
  }
}

resource "vault_approle_auth_backend_role_secret_id" "my-service" {
  backend   = vault_auth_backend.approle.path
  role_name = vault_approle_auth_backend_role.my-service.role_name
  cidr_list = ["0.0.0.0/0"]
}

resource "local_file" "role_id" {
  content         = vault_approle_auth_backend_role.my-service.role_id
  filename        = "${path.module}/role_id"
  file_permission = "0644"
}

resource "local_file" "secret_id" {
  content         = vault_approle_auth_backend_role_secret_id.my-service.secret_id
  filename        = "${path.module}/secret_id"
  file_permission = "0644"
}

resource "vault_mount" "kv" {
  path = "kv"
  type = "kv"
}

resource "vault_generic_secret" "my-service" {
  path = "kv/my-service"

  data_json = jsonencode({
    property             = "value"
    some_other_property  = "another value"
    yet_another_property = "still yet another value"
  })

  depends_on = [
    vault_mount.kv
  ]
}
