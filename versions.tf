terraform {
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }

    vault = {
      source  = "hashicorp/vault"
      version = "3.0.0"
    }
  }
}

provider "vault" {
  token = "root"
}
