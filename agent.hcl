# First, you specify where Vault is and how to authenticate

vault {
  address = "http://127.0.0.1:8200"
}

auto_auth {
  method "approle" {
    config {
      role_id_file_path   = "./role_id"
      secret_id_file_path = "./secret_id"

      remove_secret_id_file_after_reading = false
    }
  }
}

# I used AppRole because it was easier for the POC, but
# your use case would look like this...
#
# auto_auth {
#   method "kubernetes" {
#     config {
#       role = "some-role"
#     }
#   }
# }

# Finally, you specify the template to write and where to render it
template {
  source      = "properties"
  destination = "/tmp/my-service.properties"
}

# Exit after rendering the template
exit_after_auth = true
