# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.0.0"
  constraints = "3.0.0"
  hashes = [
    "h1:1Pl0vIs03udKkvtGp8m0lues4VLG2ZxlbKVa2hZsH0w=",
    "zh:0791d3bfb15351118b7fc76188348daf0c8fba5ce9163f928b1d70d1735bda17",
    "zh:2e3bea7b8d6a4535e5ea56012f34ae4aca55699dfc2622134bf485834251aaef",
    "zh:39163eeaf632f36d20e2259aad06d6df15912013bc596ac0057a5e4fbde9dccc",
    "zh:4d11763903e97ff2ec84b1752b0371ebd27371deb9fe2d38c8a449fd255814d6",
    "zh:5d5adc425d911b5cdad0a46ed1edb86160a22aacd66aeb479ce1472a5b369ad0",
    "zh:708a73f0146d2b708302816c1eda550609a288846aa5c5831b79c3ef905ae5b9",
    "zh:7a843831bf6547fa8ca4be425f16d9556f22e263b8678a9fc67110dbc84fb927",
    "zh:88f215a08d537a10a4aa313f4b819ee10107931e7b86e8652da48bb4fe57a2c2",
    "zh:f2f1ec5c22d6300dee952c863e1b245383c0b6c09610aebbe11cc0b08e62162d",
    "zh:f649701233c55874c2a259cf3bca71462780de8ed9aa06e5fd01c2f4ded4ff6c",
    "zh:f8065795c5ba87c7c815e1add725290631c6126487d5b5f3796ce8e77f623fdc",
  ]
}
